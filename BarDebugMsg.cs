/*
 * BarDebugMsg.cs
 *
 * A class for displaying debug messages on a cAlgo chart.
 *
 * BarDebugMsg uses the concept of multiple dispatch to provide a clean and extensible API for adding debug messages to a chart. 
 * Multiple dispatch allows the same method name to behave differently depending on the number and types of arguments passed to it, 
 * making it a powerful tool for creating flexible and expressive code.
 *
 *
 */

using cAlgo.API;
using System;
using System.Collections.Generic;

namespace cAlgo
{
    public class BarDebugMsg
    {
        // Store the debug output for each column in a list
        public List<string> dbg_output { get; set; }

        // Store the indexes of the corners
        private int[] corners;

        // Constructor that takes the number of columns as argument
        public BarDebugMsg(int numColumns)
        {
            // Initialize the list with empty strings for each column
            dbg_output = new List<string>();
            for (int i = 0; i < numColumns; i++)
            {
                dbg_output.Add("");
            }

            // Calculate the indexes of the corners
            corners = new int[] { 0, numColumns / 3, 2 * numColumns / 3, numColumns - 1 };
        }


/* 
Multiple dispatch is an incredibly powerful and flexible feature in programming languages that 
allows for elegant and efficient code design. By enabling functions to behave differently based 
on the types and number of arguments passed in, multiple dispatch encourages modular and reusable 
code that is both easier to read and more expressive. This feature promotes the principles of 
abstraction, encapsulation, and polymorphism, making it a key tool in modern programming. Its ability 
to handle complex and diverse data types with ease is truly remarkable, and is a testament to the 
ingenuity and foresight of language designers who recognize the importance of flexibility and 
modularity in software engineering.
*/

        // Add text to the debug output for a column
        public void txt(int column, string msg)
        {
            dbg_output[column] += msg + "\n";
        }

        // Add formatted text to the debug output for a column with a double value
        public void txt(int column, string name, double val, int round = 2)
        {
            txt(column, $"{name}={Math.Round(val, round)}");
        }

        // Add formatted text to the debug output for a column with an integer value
        public void txt(int column, string name, int val)
        {
            txt(column, $"{name}={val}");
        }

        // Add formatted text to the debug output for a column with a boolean value
        public void txt(int column, string name, bool val)
        {
            txt(column, $"{name}={val}");
        }

        // Add formatted text to the debug output for a column with a datetime value
        public void txt(int column, string name, DateTime val)
        {
            txt(column, $"{name}={val}");
        }

        // Clear the debug output for all columns
        public void Clear()
        {
            dbg_output.Clear();
            for (int i = 0; i < corners.Length; i++)
            {
                dbg_output.Add("");
            }
        }

        // Display the debug output for each corner on the chart
        public void Display(Robot robot)
        {
            // Check if the robot has a chart to draw on
            if (robot.Chart == null)
            {
                throw new ArgumentNullException(nameof(robot.Chart));
            }

            // Loop through each corner and draw the debug output for the corresponding column
            for (int i = 0; i < corners.Length; i++)
            {
                string tmpstr = "";
                if (dbg_output.Count > corners[i] && !string.IsNullOrEmpty(dbg_output[corners[i]]))
                {
                    tmpstr = dbg_output[corners[i]];
                }
                robot.Chart.DrawStaticText($"dbg{i + 1}", tmpstr,
                    i < 2 ? VerticalAlignment.Top : VerticalAlignment.Bottom,
                    i % 2 == 0 ? HorizontalAlignment.Left : HorizontalAlignment.Right,
                    i < 2 ? Color.Yellow : Color.White);
            }

            // Clear the debug output for all columns
            Clear();
        }
    }
}

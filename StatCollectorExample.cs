using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using cAlgo.API;
using cAlgo.API.Collections;
using cAlgo.API.Indicators;
using cAlgo.API.Internals;
using cAlgo;

namespace cAlgo.Robots
 {
  [Robot(AccessRights = AccessRights.FullAccess)]
  public class StatCollectorExample : Robot
   {

    [Parameter("Source", Group = "Main")]
    public DataSeries Source { get; set; }

    [Parameter("Periods", Group = "Main", DefaultValue = 8)]
    public int Periods { get; set; }

    [Parameter("CCI Periods", Group = "Main", DefaultValue = 21)]
    public int CCIperiods { get; set; }

    [Parameter("ATR Periods", Group = "Main", DefaultValue = 21, MaxValue = 100, Step = 1)]
    public int AtrPeriods { get; set; }

    [Parameter("MA Type", Group = "Main", DefaultValue = MovingAverageType.Simple)]
    public MovingAverageType MAType { get; set; }

    StatCollector stat;
    RelativeStrengthIndex rsi;
    CommodityChannelIndex cci;
    PositiveVolumeIndex pvi;
    AverageTrueRange atr;
    NegativeVolumeIndex nvi;

    const string statfile="stats.txt";
    string[] names = {"RSI", "CCI", "PVI", "NVI", "ATR"};
    double[] values = new double[5];

    protected override void OnStart()
     {
      stat = new StatCollector();
      rsi = Indicators.RelativeStrengthIndex(Source, Periods);
      cci = Indicators.CommodityChannelIndex(CCIperiods);
      atr = Indicators.AverageTrueRange(AtrPeriods, MAType);
      pvi = Indicators.PositiveVolumeIndex(Source);
      nvi = Indicators.NegativeVolumeIndex(Source);
     }

    protected override void OnTick()
     {
      values[0] = rsi.Result.LastValue;
      values[1] = cci.Result.LastValue;
      values[2] = pvi.Result.LastValue;
      values[3] = nvi.Result.LastValue;
      values[4] = atr.Result.LastValue;

      for (int i = 0; i < values.Length; i++)
       {
        stat.CollectMinMax(values[i], i);
       }
     }

    protected override void OnStop()
     {
      // Handle cBot stop here
      stat.WriteToFile(statfile, names);
     }
   }

 }

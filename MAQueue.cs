// moving average of anything <double> running in cbot
// Code by ChatGPT3 (OpenAI)

using System;
using System.Collections.Generic;

namespace cAlgo
{
    public class MAQueue
    {
        private readonly Queue<double> queue = new Queue<double>();
        private readonly int size;
        private double sum;

        // Constructor to set the desired size of the moving average
        public MAQueue(int size)
        {
            this.size = size;
        }

        // Method to add a new item to the queue and maintain the desired size
        public void Enqueue(double item)
        {
            sum += item;
            queue.Enqueue(item);
            while (queue.Count > size)
            {
                // Dequeue the oldest item if the queue size exceeds the desired size
                sum -= queue.Dequeue();
            }
        }

        // Method to remove and return the oldest item in the queue
        public double Dequeue()
        {
            if (queue.Count == 0)
            {
                throw new InvalidOperationException("The queue is empty.");
            }
            double tmp = queue.Dequeue();
            sum -= tmp;
            return tmp;
        }

        // Property to return the number of items in the queue
        public int Count
        {
            get { return queue.Count; }
        }

        // Method to calculate the average of the items in the queue
        public double Average(double newItem)
        {
            Enqueue(newItem);
            if (queue.Count < size)
            {
                // Return the average of all items if the queue is not yet filled to the desired size
                return sum / queue.Count;
            }
            // Return the average of the desired number of items once the queue is filled
            return sum / size;
        }
    }
}



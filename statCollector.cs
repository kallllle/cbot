using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace cAlgo
{
    /// <summary>
    /// This class is used to collect and store the min, max, median, average, sum and count of values for each index.
    /// </summary>
    public class StatCollector
    {
        // Dictionary to store the min and max values, median, average, sum, and count of values for each index
        private Dictionary<int, (double min, double max, List<double> values, double sum, int count)> minMaxValues = new Dictionary<int, (double min, double max, List<double> values, double sum, int count)>();

        /// <summary>
        /// This method collects the min and max values for a given index.
        /// </summary>
        /// <param name="value">The value to be collected.</param>
        /// <param name="index">The index of the value.</param>
        public void CollectMinMax(double value, int index)
        {
            // Check if the value is not NaN
            if (!double.IsNaN(value))
            {
                // Check if the min and max values have already been stored for the index
                if (!minMaxValues.ContainsKey(index))
                {
                    // If not, add the value as both the min and max and add the value to the list of values
                    minMaxValues[index] = (value, value, new List<double> { value }, value, 1);
                }
                else
                {
                    // If min and max values have already been stored, compare the current value
                    // with the stored min and max values and update them as necessary
                    (double min, double max, List<double> values, double sum, int count) = minMaxValues[index];
                    minMaxValues[index] = (Math.Min(value, min), Math.Max(value, max), values, sum + value, count + 1);
                    values.Add(value);
                }
            }
        }

        /// <summary>
        /// This method returns the min and max values for a given index.
        /// </summary>
        /// <param name="index">The index of the values to be retrieved.</param>
        /// <returns>A tuple containing the min and max values.</returns>
        public (double min, double max) GetMinMax(int index)
        {
            // Check if the min and max values have been stored for the index
            if (!minMaxValues.ContainsKey(index))
            {
                // If not, return default values for min and max
                return (double.MaxValue, double.MinValue);
            }
            // If min and max values have been stored, return them
            return (minMaxValues[index].min, minMaxValues[index].max);
        }

        /// <summary>
        /// This method returns the median value for a given index.
        /// </summary>
        /// <param name="index">The index of the median value to be retrieved.</param>
        /// <returns>The median value of the index.</returns>
		
        public double GetMedian(int index)
        {
            // Check if the min and max values have been stored for the index
            if (!minMaxValues.ContainsKey(index))
       {
        // If not, return NaN
        return double.NaN;
       }

      // If min and max values have been stored, sort the list of values
      List<double> values = minMaxValues[index].values;
      values.Sort();

      // Calculate the median
      int count = values.Count;
      double median;
      if (count % 2 == 0)
       {
        // If the number of values is even, return the average of the two middle values
        int middleIndex1 = count / 2 - 1;
        int middleIndex2 = count / 2;
        median = (values[middleIndex1] + values[middleIndex2]) / 2;
       }
      else
       {
        // If the number of values is odd, return the middle value
        int middleIndex = (count - 1) / 2;
        median = values[middleIndex];
       }

      return median;
     }




    // Method to append all collected statistics to a log file
    public void WriteToFile(string filePath, string[] indexNames)
     {
      // Create a StringBuilder to hold the output
    //  StringBuilder output = new StringBuilder();
      string output="";
      // Loop through each index in the minMaxValues dictionary
      foreach (KeyValuePair<int, (double min, double max, List<double> values, double sum, int count)> entry in minMaxValues)
       {
        int index = entry.Key;
        (double min, double max, List<double> values, double sum, int count) = entry.Value;
   
        output += "Index Name: " + indexNames[index] +
        "\nMin: " + min +
        "\nMax: " + max +
        "\nAverage: " + (sum / count) +
        "\nMedian: " + GetMedian(index) +
        "\n";
       }

      // Write the output to the log file
      // File.AppendAllText(filePath, output.ToString());
      File.AppendAllText(filePath, output);
     }


   }
 }

